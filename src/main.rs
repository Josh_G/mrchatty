use log::{info, warn};
use reqwest::blocking::{get, Client};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    error::Error,
    fs::File,
    io::{Read, Write},
};

const CTFTIME_API: &str = "https://ctftime.org/api/v1/teams/";

const HOOK_KEY: &str = "SLACK_WEBHOOK";
const TEAM_KEY: &str = "TEAM_ID";
const SAVE_FILENAME: &str = "position.json";
const RATING_EPSILON: f64 = 1.0;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct CTFRating {
    organizer_points: u64,
    rating_points: f64,
    rating_place: u64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SlackMessage {
    text: String,
}

impl CTFRating {
    pub fn empty() -> CTFRating {
        CTFRating {
            organizer_points: 0,
            rating_points: 0.0,
            rating_place: 99999,
        }
    }

    pub fn load() -> CTFRating {
        match File::open(SAVE_FILENAME) {
            Err(err) => {
                warn!("error opening old place file: {}", err);
                CTFRating::empty()
            }
            Ok(mut rating_file) => {
                let mut contents = String::new();

                rating_file.read_to_string(&mut contents).unwrap();
                match serde_json::from_str(&contents) {
                    Err(err) => {
                        warn!("error getting old place: {}", err);
                        CTFRating::empty()
                    }
                    Ok(rating) => rating,
                }
            }
        }
    }

    pub fn save(&self) -> Result<(), Box<dyn Error>> {
        let mut position_file: File = File::create(SAVE_FILENAME)?;
        let serialised = serde_json::to_string(self)?;
        position_file.write_all(serialised.as_bytes())?;

        Ok(())
    }
}

#[derive(Deserialize, Debug)]
pub struct CTFRatings {
    // Array of year=>CTFRating results
    rating: Vec<HashMap<u32, CTFRating>>,
}

pub fn get_team_place(team: &str) -> Result<CTFRating, Box<dyn Error>> {
    let url: String = format!("{}{}/", CTFTIME_API, team);

    let places: CTFRatings = get(&url)?.json()?;

    // The current year is always the first element
    let current_place: CTFRating = places.rating[0]
        .iter()
        .take(1)
        .map(|(_year, rating)| rating)
        .collect::<Vec<&CTFRating>>()[0]
        .clone();

    Ok(current_place)
}

fn post_message_to_slack(msg: &str, webhook: &str) -> Result<(), Box<dyn Error>> {
    let client = Client::new();

    let msg = SlackMessage {
        text: String::from(msg),
    };
    client.post(webhook).json(&msg).send()?;

    Ok(())
}

fn main() {
    env_logger::init();
    dotenv::dotenv().ok();
    info!("Getting current team place!");

    // TODO: Better error handling here
    let slack_webhook: String =
        dotenv::var(HOOK_KEY).expect(&format!("Need a key for {} in .env", HOOK_KEY));
    let team_id: String =
        dotenv::var(TEAM_KEY).expect(&format!("Need a key for {} in .env", TEAM_KEY));
    let current_place = get_team_place(&team_id).expect("Couldn't get team place!");

    info!("Currently we're: {}", current_place.rating_place);

    let old_place = CTFRating::load();

    if current_place.rating_place != old_place.rating_place
        && (current_place.rating_points - old_place.rating_points).abs() > RATING_EPSILON
    {
        info!(
            "Different placing!: {}->{}",
            old_place.rating_place, current_place.rating_place
        );

        //TODO: This macro stuff is horrible, const strings wen
        let msg = format!(
            r#"------- :rotating_light: CTFTIME ALERT :rotating_light: -------
    :earth_africa::earth_africa::earth_africa: We moved from position _{}_ to *{}* in the world! :earth_africa::earth_africa::earth_africa:
    We have {} points!"#,
            old_place.rating_place, current_place.rating_place, current_place.rating_points
        );

        post_message_to_slack(&msg, &slack_webhook).unwrap();
        current_place.save().unwrap();
    } else {
        info!("No change in place: {}", current_place.rating_place);
    }
}
