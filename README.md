# MrChatty

Need to motivate your CTF team to play more glorious CTFs? Boy do we have the solution for you! MrChatty is an all in one webhook based Slack bot that will stalk your team's ranking on CTFtime 24/7 then post it to your Slack if it changes!

## Quickstart

```sh
git clone <repo>
cargo build
echo "SLACK_WEBHOOK=<team_webook>\nTEAM_ID=<ctftime_id>" > .env
./build/release/mrchatty
```

Where:

ctftime_id = The numerical id in the URL of your team's profile page  
team_webhook = Slack webhook set to post into a channel

`.env` must be located in the same directory that you expect to run `mrchatty` from

## Features

1. Overengineered, why is it even in Rust?
2. Annoying. Don't get me wrong, fun the first couple of messages but irritating afterwards
3. It's written in Rust
4. Rust
5. CTF
